/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2openstack;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

import de.ugoe.cs.rwm.tocci.AbsTransformator;

/**
 * Class implementing the OCCI2OCCIRUNTIMECONFIG transformation.
 *
 * @author Johannes Erbel
 *
 */
public class OCCI2OPENSTACKTransformator extends AbsTransformator {
	private static String managementNWRuntimeId;
	private static String sshKey;
	private static String userData;
	private static String manNWid;
	private static String flavor;
	private static String image;
	private static String remoteUser;

	public static String getFlavor() {
		return flavor;
	}

	public static void setFlavor(String flavor) {
		OCCI2OPENSTACKTransformator.flavor = flavor;
	}

	public static String getImage() {
		return image;
	}

	public static void setImage(String image) {
		OCCI2OPENSTACKTransformator.image = image;
	}

	public static String getRemoteUser() {
		return remoteUser;
	}

	public static void setRemoteUser(String remoteUser) {
		OCCI2OPENSTACKTransformator.remoteUser = remoteUser;
	}

	private final static File EOLFILE = AbsTransformator
			.getTransFile("de/ugoe/cs/rwm/tocci/occi2openstack/OCCI2OPENSTACK.eol");

	public static void setManagementNWRuntimeId(String managementNWRuntimeId) {
		OCCI2OPENSTACKTransformator.managementNWRuntimeId = managementNWRuntimeId;
	}

	public static void setSshKey(String sshKey) {
		OCCI2OPENSTACKTransformator.sshKey = sshKey;
	}

	public static void setUserData(String userData) {
		OCCI2OPENSTACKTransformator.userData = userData;
	}

	public static void setManNWid(String manNWid) {
		OCCI2OPENSTACKTransformator.manNWid = manNWid;
	}

	public void setTransformationProperties(String manNWRuntimeId, String sshKey, String userData, String manNWid,
			String flavor, String image, String remoteUser) {
		setManNWid(manNWid);
		setUserData(userData);
		setSshKey(sshKey);
		setManagementNWRuntimeId(manNWRuntimeId);
		setFlavor(flavor);
		setImage(image);
		setRemoteUser(remoteUser);
	}

	public void setTransformationProperties(String manNWRuntimeId, String sshKey, String userData, String manNWid) {
		setManNWid(manNWid);
		setUserData(userData);
		setSshKey(sshKey);
		setManagementNWRuntimeId(manNWRuntimeId);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path occiModelPath, Path newocciModelPath) throws EolRuntimeException {
		try {
			Files.copy(occiModelPath, newocciModelPath, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Resource res = loadOCCIintoEMFResource(occiModelPath);
		transform(res, newocciModelPath);

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path inputPath, Path outputPath, Path additionalPath) throws EolRuntimeException {
		return transform(inputPath, outputPath);
	}

	@Override
	public String transform(Resource inputModel, Path outputPath) throws EolRuntimeException {
		IEolModule module = eolModuleSetup(EOLFILE);

		InMemoryEmfModel occiModel = new InMemoryEmfModel("OCCI", inputModel);

		ResourceSet extResourceSet = new ResourceSetImpl();
		URI openstackRuntime = URI.createURI(
				OcciRegistry.getInstance().getExtensionURI("http://schemas.modmacao.org/openstack/runtime#"));
		Resource openstackRuntimeResource = extResourceSet.getResource(openstackRuntime, true);
		InMemoryEmfModel openstackRuntimeModel = new InMemoryEmfModel("OpenstackRuntime", openstackRuntimeResource);

		URI ansible = URI
				.createURI(OcciRegistry.getInstance().getExtensionURI("http://schemas.modmacao.org/occi/ansible#"));
		Resource ansibleResource = extResourceSet.getResource(ansible, true);
		InMemoryEmfModel ansibleModel = new InMemoryEmfModel("Ansible", ansibleResource);

		module.getContext().getModelRepository().addModel(ansibleModel);
		module.getContext().getModelRepository().addModel(occiModel);
		module.getContext().getModelRepository().addModel(openstackRuntimeModel);

		module.execute();
		occiModel.store();
		// TODO: Store OCCI Configuration at additionalPath
		return null;
	}

	@Override
	public String transform(Resource sourceModel, Resource targetModel, Path outputPath) throws EolRuntimeException {
		return transform(sourceModel, outputPath);
	}

	public String getManagementNWRuntimeId() {
		return managementNWRuntimeId;
	}

	public String getSshKey() {
		return sshKey;
	}

	public String getUserData() {
		return userData;
	}

	public String getManNWid() {
		return manNWid;
	}

}
