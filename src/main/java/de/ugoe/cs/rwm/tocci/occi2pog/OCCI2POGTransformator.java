/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2pog;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;

import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.models.IModel;

import de.ugoe.cs.rwm.tocci.AbsTransformator;
import pog.PogPackage;

/**
 * Class implementing the OCCI2POG transformation.
 *
 * @author Johannes Erbel
 *
 */
public class OCCI2POGTransformator extends AbsTransformator {
	private final static File ETLFILE = AbsTransformator.getTransFile("de/ugoe/cs/rwm/tocci/occi2pog/OCCI2POG.etl");

	public OCCI2POGTransformator() {
		PogPackage.eINSTANCE.eClass();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path occiModelPath, Path pogModelPath) throws EolRuntimeException {

		IEolModule module = etlModuleSetup(ETLFILE);
		try {
			String occiURI = "http://schemas.ogf.org/occi/core/ecore";
			IModel occiModel = createEmfModel("OCCI", occiModelPath.toAbsolutePath().toString(), occiURI, true, false);

			String pogURI = "http://swe.simpaas.pog.de/pog";
			IModel pogModel = createEmfModel("POG", pogModelPath.toAbsolutePath().toString(), pogURI, false, true);

			module.getContext().getModelRepository().addModel(pogModel);
			module.getContext().getModelRepository().addModel(occiModel);

			module.execute();
			pogModel.store();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path inputPath, Path outputPath, Path additionalPath) throws EolRuntimeException {
		return transform(inputPath, outputPath);
	}

	@Override
	public String transform(Resource inputModel, Path outputPath) {
		IEolModule module = etlModuleSetup(ETLFILE);

		try {
			InMemoryEmfModel occiModel = new InMemoryEmfModel("OCCI", inputModel, OCCIPackage.eINSTANCE);
			occiModel.getAliases().add("OCCI");

			URI uri = URI.createFileURI(outputPath.toString());
			String pogURI = "http://swe.simpaas.pog.de/pog";
			IModel pogModel = createEmfModel("POG", uri, pogURI, false, true);

			module.getContext().getModelRepository().addModel(pogModel);
			module.getContext().getModelRepository().addModel(occiModel);

			Object result = module.execute();
			pogModel.store();
			return result.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String transform(Resource sourceModel, Resource targetModel, Path outputPath) {
		return transform(sourceModel, outputPath);
	}
}
