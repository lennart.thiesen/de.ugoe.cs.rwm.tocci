/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.pcg2ipg;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.models.IModel;
import org.eclipse.epsilon.flock.FlockModule;

import de.ugoe.cs.rwm.tocci.AbsTransformator;
import pcg.PcgPackage;

public class PCG2IGPTransformator extends AbsTransformator {

	private final static File FLOCKFILE = AbsTransformator.getTransFile("de/ugoe/cs/rwm/tocci/pcg2ipg/PCG2IPG.mig");

	public PCG2IGPTransformator() {
		PcgPackage.eINSTANCE.eClass();
	}

	@Override
	public String transform(Path pcgPath, Path ipgPath) throws EolRuntimeException {
		FlockModule module = flockModuleSetup(FLOCKFILE);
		try {

			URI uri = URI.createFileURI(pcgPath.toString());
			String pcgURI2 = "http://swe.simpaas.pcg.de/pcg";
			IModel pcg = createEmfModel("PCG", uri, pcgURI2, true, false);

			uri = URI.createFileURI(ipgPath.toString());
			String ipgURI = "http://swe.simpaas.pcg.de/pcg";
			IModel ipg = createEmfModel("IPG", uri, ipgURI, false, true);

			module.getContext().getModelRepository().addModel(pcg);
			module.getContext().getModelRepository().addModel(ipg);
			Object result = module.execute(pcg, ipg);
			ipg.store();
			return result.toString();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String transform(Path inputPath, Path outputPath, Path additionalPath) throws EolRuntimeException {
		return transform(inputPath, outputPath);
	}

	@Override
	public String transform(Resource inputModel, Path outputPath) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String transform(Resource sourceModel, Resource targetModel, Path outputPath) {
		return transform(sourceModel, outputPath);
	}

}
