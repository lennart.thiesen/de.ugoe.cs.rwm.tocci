/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2pcg;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;

import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.models.IModel;

import de.ugoe.cs.rwm.tocci.AbsTransformator;
import pcg.PcgPackage;

public class OCCI2PCGTransformator extends AbsTransformator {

	private final static File ETLFILE = AbsTransformator.getTransFile("de/ugoe/cs/rwm/tocci/occi2pcg/OCCI2PCG.etl");

	public OCCI2PCGTransformator() {
		PcgPackage.eINSTANCE.eClass();
	}

	@Override
	public String transform(Path oldModelPath, Path newModelPath, Path pcgPath) throws EolRuntimeException {
		IEolModule module = etlModuleSetup(ETLFILE);

		try {
			String occiURI = "http://schemas.ogf.org/occi/core/ecore";
			IModel oldOCCI = createEmfModel("srcOCCI", oldModelPath.toAbsolutePath().toString(), occiURI, true, false);

			IModel newOCCI = createEmfModel("tarOCCI", newModelPath.toAbsolutePath().toString(), occiURI, true, false);

			String pcgURI = "http://swe.simpaas.pcg.de/pcg";
			IModel pcgModel = createEmfModel("PCG", pcgPath.toAbsolutePath().toString(), pcgURI, false, true);

			module.getContext().getModelRepository().addModel(oldOCCI);
			module.getContext().getModelRepository().addModel(newOCCI);
			module.getContext().getModelRepository().addModel(pcgModel);

			Object result = module.execute();
			pcgModel.store();

			return result.toString();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String transform(Resource sourceModel, Resource targetModel, Path outputPath) {
		IEolModule module = etlModuleSetup(ETLFILE);
		try {
			String pcgURI = "http://swe.simpaas.pcg.de/pcg";

			URI uri = URI.createFileURI(outputPath.toString());

			IModel pcgModel = createEmfModel("PCG", uri, pcgURI, false, true);

			InMemoryEmfModel source = new InMemoryEmfModel("srcOCCI", sourceModel, OCCIPackage.eINSTANCE);
			InMemoryEmfModel target = new InMemoryEmfModel("tarOCCI", targetModel, OCCIPackage.eINSTANCE);
			source.getAliases().add("srcOCCI");
			target.getAliases().add("tarOCCI");

			module.getContext().getModelRepository().addModel(source);
			module.getContext().getModelRepository().addModel(target);
			module.getContext().getModelRepository().addModel(pcgModel);

			Object result = module.execute();
			pcgModel.store();
			// pcgModel.store(outputPath.toString());

			return result.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String transform(Resource inputModel, Path outputModel) {
		return null;
	}

	@Override
	public String transform(Path oldOCCI, Path newOCCI) {
		return null;
	}
}
