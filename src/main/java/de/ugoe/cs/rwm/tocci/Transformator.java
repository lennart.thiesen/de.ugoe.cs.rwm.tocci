/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci;

import java.nio.file.Path;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

/**
 * Interfaces handling the transformation of models.
 *
 * @author Johannes Erbel
 *
 */
public interface Transformator {
	static final Logger LOG = Logger.getLogger(Transformator.class.getName());

	/**
	 * Transformation of a model stored at the inputPath to a model stored in the
	 * outputPath.
	 *
	 * @param inputPath
	 *            Path to the file containing the input model for the
	 *            transformation.
	 * @param outputPath
	 *            Path to the file containing the output model for the
	 *            transformation.
	 * @return result of the transformation
	 * @throws EolRuntimeException
	 *             Exception thrown when the transformation fails.
	 */
	public String transform(Path inputPath, Path outputPath) throws EolRuntimeException;

	public String transform(Resource inputModel, Path outputModel) throws EolRuntimeException;

	/**
	 * Transformation of a model stored at the inputPath to a model stored in the
	 * outputPath. For the information additional information is required contained
	 * in the model stored in the additionalPath.
	 *
	 * @param inputPath
	 *            Path to the file containing the input model for the
	 *            transformation.
	 * @param outputPath
	 *            Path to the file containing the output model for the
	 *            transformation.
	 * @param additionalPath
	 *            Path to the file containing an additional model required for the
	 *            transformation.
	 * @return result of the transformation
	 * @throws EolRuntimeException
	 *             Exception thrown when the transformation fails.
	 */
	public String transform(Path inputPath, Path outputPath, Path additionalPath) throws EolRuntimeException;

	public String transform(Resource sourceModel, Resource targetModel, Path outputPath) throws EolRuntimeException;

}
