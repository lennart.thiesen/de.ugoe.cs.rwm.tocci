/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci;

import de.ugoe.cs.rwm.tocci.design2reqruntime.DESIGN2REQRUNTIMETransformator;
import de.ugoe.cs.rwm.tocci.occi2occi.OCCI2OCCITransformator;
import de.ugoe.cs.rwm.tocci.occi2pcg.OCCI2PCGTransformator;
import de.ugoe.cs.rwm.tocci.occi2pog.OCCI2POGTransformator;
import de.ugoe.cs.rwm.tocci.pcg2ipg.PCG2IGPTransformator;
import de.ugoe.cs.rwm.tocci.pog2provplan.POG2ProvPlanTransformator;

public class TransformatorFactory {
	public static Transformator getTransformator(String criteria) {
		if (criteria.equals("OCCI2POG")) {
			return new OCCI2POGTransformator();
		}
		if (criteria.equals("POG2ProvPlan")) {
			return new POG2ProvPlanTransformator();
		}
		if (criteria.equals("OCCI2PCG")) {
			return new OCCI2PCGTransformator();
		}
		if (criteria.equals("PCG2IPG")) {
			return new PCG2IGPTransformator();
		}
		if (criteria.equals("OCCI2OCCI")) {
			return new OCCI2OCCITransformator();
		}
		if (criteria.equals("DESIGN2REQRUNTIME")) {
			return new DESIGN2REQRUNTIMETransformator();
		}
		throw new IllegalArgumentException(
				"Transformator: " + criteria + " does not exist." + "Supported Transformators:"
						+ " OCCI2POG; POG2ProvPlan; OCCI2PCG; PCG2IPG; OCCI2OCCI; DESIGN2REQRUNTIME");
	}
}
