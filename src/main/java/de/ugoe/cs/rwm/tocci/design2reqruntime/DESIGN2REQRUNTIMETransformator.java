/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.design2reqruntime;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

import de.ugoe.cs.rwm.tocci.AbsTransformator;

/**
 * Class implementing the OCCI2POG transformation.
 *
 * @author Johannes Erbel
 *
 */
public class DESIGN2REQRUNTIMETransformator extends AbsTransformator {
	private final static File EOLFILE = AbsTransformator
			.getTransFile("de/ugoe/cs/rwm/tocci/design2reqruntime/DESIGN2REQRUNTIME.eol");

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path occiModelPath, Path newocciModelPath) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path designtimePath, Path runtimePath, Path reqRuntimePath) throws EolRuntimeException {
		Resource runtimeModel = loadOCCIintoEMFResource(runtimePath);
		Resource designModel = loadOCCIintoEMFResource(designtimePath);
		transform(designModel, runtimeModel, reqRuntimePath);

		return null;
	}

	@Override
	public String transform(Resource inputModel, Path outputPath) {
		return null;
	}

	@Override
	public String transform(Resource designModel, Resource targetModel, Path outputPath) throws EolRuntimeException {

		URI adjustedURI = URI.createFileURI(outputPath.toString());
		designModel.setURI(adjustedURI);
		try {
			designModel.save(Collections.EMPTY_MAP);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		IEolModule module = eolModuleSetup(EOLFILE);

		Resource reqruntimeModel = loadOCCIintoEMFResource(outputPath);

		InMemoryEmfModel runOCCI = new InMemoryEmfModel("runOCCI", targetModel);
		InMemoryEmfModel reqRunOCCI = new InMemoryEmfModel("reqOCCI", reqruntimeModel);

		module.getContext().getModelRepository().addModel(runOCCI);
		module.getContext().getModelRepository().addModel(reqRunOCCI);

		module.execute();

		reqRunOCCI.store();
		return null;
	}
}
