//################################################################################
//################################################################################
//################################################################################
//################################################################################
//##########################      MAIN           #################################
//################################################################################
//################################################################################
//################################################################################
//################################################################################
var trans = new Native("de.ugoe.cs.rwm.tocci.design2reqruntime.DESIGN2REQRUNTIMETransformator");
//Has to be performed first
trans.logInfo("Updating Task States");
updateTaskStates();
var fTasks = finishedTasks();
trans.logInfo("Tasks finished: " + printTaskSet(fTasks));
var wTasks = waitingTasks();
trans.logInfo("Tasks waiting: " + printTaskSet(wTasks));
var eTasks = executingTasks();
trans.logInfo("Tasks for execution: " + printTaskSet(eTasks));
var sTasks = skippedTasks();
trans.logInfo("Tasks skipped: " + printTaskSet(sTasks));

trans.logDebug("Calculating Resources to be deleted");
var toBeDeleted = new Set();
var retained = new Set();
removeUnrequiredApplications();

trans.logDebug("Retaining Resources: " + printTaskSet(retained)); 
trans.logInfo("Deleting Resources: " + printTaskSet(toBeDeleted));
for(ent in toBeDeleted) {
    trans.logDebug("Deleting: "+ ent.title);
    ent.removeLinksAndMixins();
}

//################################################################################
//################################################################################
//################################################################################
//################################################################################
//##########################     Operations      #################################
//################################################################################
//################################################################################
//################################################################################
//################################################################################

operation Any removeLinksAndMixins() {
		while (not self.links.isEmpty()) {
		 trans.logDebug("        Removing Link: " + self.links.first.title);
          delete self.links.first;
		}
		while (not self.parts.isEmpty()) {
         trans.logDebug("        Removing Mixin: " + self.parts.first);
          delete self.parts.first;
        }
		delete self;
}

operation Any removeLinks() {
        while (not self.links.isEmpty()) {
         trans.logDebug("        Removing Link: " + self.links.first.title);
          delete self.links.first;
        }
}

operation Any removePlatformLinks() {

    for(link in self.links.clone) {
        if(link.isKindOf(reqOCCI!Taskdependency) == false) {
            delete link;
            }
            
    }
}


operation reqOCCI!Task readyForExec(): Boolean {
    if(self.isSkipped()){
        return false;
    }
    if(self.isFinished()) {
        if(self.dependenciesFinished() == false){
           return true;
        }
        return false;
    }
    var ready = false;
    for(link in self.rlinks) {
        if(link.isKindOf(runOCCI!Taskdependency)){
            var srcTask = link.source;
            if(link.taskDependencyIsloose == false){
                if(srcTask.isFinished() == false and srcTask.isSkipped() == false) {
                    return false;
                }
            }
        }
    }
        
    return true;
}

operation updateTaskStates() {
    for(reqTask in reqOCCI!Task){
        for(runTask in runOCCI!Task) {
            if(reqTask.id == runTask.id) {
                reqTask.workflowTaskState = runTask.workflowTaskState;
            }
        }
        for(link in reqTask.links){
            if(link.isKindOf(reqOCCI!Datalink)){
                for(dLink in runOCCI!Datalink) {
                    if(dLink.id == link.id) {
                        link.taskDatalinkState = dLink.taskDatalinkState;
                    }
                }
            }
        }
    }

}

operation reqOCCI!Task isFinished() : Boolean {
/*
    for(link in self.links){
        if(link.isKindOf(runOCCI!Taskdependency)){
            if(link.taskDependencyIsloose == true){
                var tar = link.target;
                if(tar.isFinished()){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
    }*/
    if(self.workflowTaskState.toString() == "finished") {
        return true;
    }
    return false;
}

operation waitingTasks(): Set {
    var waitingTasks = new Set;
    for(task in reqOCCI!Task){
        if(task.readyForExec() == false and task.isFinished() == false and task.isSkipped() == false){
            waitingTasks.add(task);
        }
    }
    return waitingTasks;
}

operation finishedTasks(): Set {
    var finishedTasks = new Set;
    for(task in reqOCCI!Task){
        if(task.isFinished()){
            finishedTasks.add(task);
        }
    }
    return finishedTasks;
}

operation skippedTasks(): Set {
    var skippedTasks = new Set;
    for(task in reqOCCI!Task){
        if(task.isSkipped()){
            skippedTasks.add(task);
        }
    }
    return skippedTasks;
}

operation reqOCCI!Task isSkipped() : Boolean {
    if(self.workflowTaskState.toString() == "skipped"){
            return true;
        }
    return false;
}

operation executingTasks(): Set {
    var executingTasks = new Set;
    for(task in reqOCCI!Task){
        if(task.readyForExec() == true){
            executingTasks.add(task);
        }
    }
    return executingTasks;
}

operation printTaskSet(set : Set) : String {
    var str = "";
    for(task in set) {
        if(task.title.isDefined()){
         str += (task.title + " | " );
        }
        else{
            for(attr in task.attributes){
                if(attr.name == "occi.core.title"){
                    str += (attr.value + " | " );
                }
            }
        }
    }
    return str;
}


operation rekRemove(res: Any) {
    if(res.links.isEmpty()) {
        return;
    }
    else{
        for(link in res.links) {
	        if(link.target.isKindOf(reqOCCI!Task) == false) {   
	            rekRemove(link.target);
	            if(link.target <> null){
	                if(link.target.isConnectedToExecutingTask() <> true) {
	                    toBeDeleted.add(link.target);
	                }
	                else{ 
	                    retained.add(link.target);
	                }
	            }
            }
        }
    }
}

//TO BE IMPROVED!!!
operation Any isConnectedToExecutingTask() : Boolean {
	    for(rLink in self.rlinks){
	        var source = rLink.source;
	        if(source.isKindOf(reqOCCI!Task)){
		        if(eTasks.contains(source)){
		           return true;
		        }
	        }
	        else {
	           var isConnected = new Set();
	           for(sourceRLinks in source.rlinks){
	              isConnected.add(source.isConnectedToExecutingTask());
	           }
	             if(isConnected.contains(true)){
	               return true;
	             }
	        }
	    }
	    return false;
}


operation removeUnrequiredApplications(){
    for(skippedTask in sTasks) {
          rekRemove(skippedTask);
          skippedTask.removePlatformLinks();
    }
    for(waitingTask in wTasks) {
          rekRemove(waitingTask);
          waitingTask.removePlatformLinks();
    }
    for(finishedTask in fTasks) {
        if(finishedTask.dependenciesFinished()){
	         rekRemove(finishedTask);
	         finishedTask.removePlatformLinks();
        }
    }
}

operation reqOCCI!Task dependenciesFinished() : Boolean {
        for(link in self.links){
           if(link.isKindOf(reqOCCI!Datalink)){
                if(link.taskDatalinkState.toString() <> "finished") {
                    trans.logInfo("Dependencies not finished: " + self.title + " : " + link.taskDatalinkState.toString());
                    return false;
                } 
           }
        }
        return true;
}
