/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2openstack;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.tocci.TestUtility;
import de.ugoe.cs.rwm.tocci.Transformator;

public class Occi2OpenstackTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));
	String manNWid = "urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590";
	String managementNWRuntimeId = "75a4639e-9ce7-4058-b859-8a711b0e2e7b";
	String sshKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZ"
			+ "G9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7Z"
			+ "QxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+Cb"
			+ "NKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBu"
			+ "xzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova";
	String userData = "I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGll"
			+ "IHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRl"
			+ "CnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBw"
			+ "YXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8"
			+ "CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBl"
			+ "bnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAK"
			+ "ICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQg"
			+ "ZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFj"
			+ "ZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5z"
			+ "MwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93"
			+ "LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1"
			+ "CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAg"
			+ "ICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhj"
			+ "cAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBl"
			+ "bnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAog"
			+ "ICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhv"
			+ "dHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAog"
			+ "ICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj";
	// String flavor = "ce8c33af-0cd5-4aac-b6f3-fcde58c4b262";
	// String image = "";
	// String remoteUser = "";

	OCCI2OPENSTACKTransformator trans = new OCCI2OPENSTACKTransformator();

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);

	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void manNWmissing() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/openstack/manNWMissing.occic"));
		trans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid);
		trans.transform(sourceOcci, targetOcci);

		assertTrue(true);
	}

	@Test
	public void manNWexistingDiffId() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/openstack/manNWexistingDiffId.occic"));
		trans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid);
		trans.transform(sourceOcci, targetOcci);

		assertTrue(true);
	}

	@Test
	public void manNWexistingDiffRuntimeId() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/openstack/manNWexistingDiffRuntimeId.occic"));
		trans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid);
		trans.transform(sourceOcci, targetOcci);
		Resource res = TestUtility.loadOCCIResource(sourceOcci);

		for (EObject obj : res.getContents().get(0).eContents()) {
			System.out.println(obj);
		}

		assertTrue(true);
	}

	@Test
	public void manNWexistingLinksMissing() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/openstack/manNWexistingLinksMissing.occic"));
		trans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid);
		trans.transform(sourceOcci, targetOcci);

		assertTrue(true);
	}

	@Test
	public void manNWexistingLinksMissing2() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/openstack/SugarCRM-Interop-Definitions.occic"));
		trans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid);
		trans.transform(sourceOcci, targetOcci);

		assertTrue(true);
	}

	@Test
	public void manNWexistingLinksExisting() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/openstack/manNWexistingLinksExisting.occic"));
		trans.transform(sourceOcci, targetOcci);

		assertTrue(true);
	}
}
