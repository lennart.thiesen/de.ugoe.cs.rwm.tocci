/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.pcg2ipg;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Test;

import de.ugoe.cs.rwm.tocci.TestUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

public class Pcg2IpgTest {

	@Test
	public void createIPG() throws EolRuntimeException {
		TestUtility.extensionRegistrySetup();

		Path pcgPath = Paths.get(TestUtility.getPathToResource("pcg/PCGsource.pcg"));
		Path ipgPath = Paths.get(TestUtility.getPathToResource("ipg/IPG.pcg"));
		Transformator pcgToIpg = TransformatorFactory.getTransformator("PCG2IPG");
		pcgToIpg.transform(pcgPath, ipgPath);

		assertTrue(true);
	}
}
