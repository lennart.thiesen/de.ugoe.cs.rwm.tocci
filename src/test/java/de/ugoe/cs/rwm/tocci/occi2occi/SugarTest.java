/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2occi;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

import de.ugoe.cs.rwm.tocci.TestUtility;

public class SugarTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Ignore
	public void sugarTest() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/SugarCRM-Interop-Definitions-migrated.occic"));
		Resource sourceRes = TestUtility.loadOCCIResource(sourceOcci);

		OCCI2OCCITransformator trans = new OCCI2OCCITransformator();
		trans.transform(sourceRes, targetOcci);

	}
}
