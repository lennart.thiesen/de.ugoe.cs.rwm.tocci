/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.design2reqruntime;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.tocci.TestUtility;
import de.ugoe.cs.rwm.tocci.Transformator;

public class Design2ReqRuntimeSharkDecisionTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.FATAL);
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void Shark_VCS() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();

		Path mlsFinished = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision2.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(mlsFinished);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		if (comp.getMissingElements().isEmpty() && comp.getNewElements().isEmpty()) {
			assertTrue(true);
		} else {
			System.out.println("Missing:");
			for (EObject obj : comp.getMissingElements()) {
				System.out.println(obj);
			}

			System.out.println("New:");
			for (EObject obj : comp.getNewElements()) {
				System.out.println(obj);
			}

			assertTrue(false);
		}
	}

	@Test
	public void Shark_Meco() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();

		Path mlsFinished = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision3.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(mlsFinished);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		if (comp.getMissingElements().isEmpty() && comp.getNewElements().isEmpty()) {
			assertTrue(true);
		} else {
			System.out.println("Missing:");
			for (EObject obj : comp.getMissingElements()) {
				System.out.println(obj);
			}

			System.out.println("New:");
			for (EObject obj : comp.getNewElements()) {
				System.out.println(obj);
			}

			assertTrue(false);
		}
	}

	@Test
	public void Shark_Decision() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision3.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();

		Path mlsFinished = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision4.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(mlsFinished);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		if (comp.getMissingElements().isEmpty() && comp.getNewElements().isEmpty()) {
			assertTrue(true);
		} else {
			System.out.println("Missing:");
			for (EObject obj : comp.getMissingElements()) {
				System.out.println(obj);
			}

			System.out.println("New:");
			for (EObject obj : comp.getNewElements()) {
				System.out.println(obj);
			}

			assertTrue(false);
		}
	}

	@Test
	public void Shark_False() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecision.occic"));
		Path runOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecisionFalse.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();

		Path mlsFinished = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkDecision/SmartSharkDecisionFalseFinal.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(mlsFinished);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		if (comp.getMissingElements().isEmpty() && comp.getNewElements().isEmpty()) {
			assertTrue(true);
		} else {
			System.out.println("Missing:");
			for (EObject obj : comp.getMissingElements()) {
				System.out.println(obj);
			}

			System.out.println("New:");
			for (EObject obj : comp.getNewElements()) {
				System.out.println(obj);
			}

			assertTrue(false);
		}
	}
}
