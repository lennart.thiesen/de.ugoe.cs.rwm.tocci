/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2pcg;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.tocci.TestUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

public class Occi2PcgTest {

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void infraPCG() throws EolRuntimeException {
		Path oldModelPath = Paths.get(TestUtility.getPathToResource("occi/Infra.occic"));
		Path newModelPath = Paths.get(TestUtility.getPathToResource("occi/Infra2.occic"));

		Transformator occiToPcg = TransformatorFactory.getTransformator("OCCI2PCG");
		Path pcgPath = Paths.get(TestUtility.getPathToResource("pcg/PCG.pcg"));
		occiToPcg.transform(oldModelPath, newModelPath, pcgPath);

		assertTrue(true);
	}

	@Test
	public void platformPCG() throws EolRuntimeException {
		Path oldModelPath = Paths.get(TestUtility.getPathToResource("occi/PaaSOld.occic"));
		Path newModelPath = Paths.get(TestUtility.getPathToResource("occi/PaaSNew.occic"));

		Resource oldRes = TestUtility.loadOCCIResource(oldModelPath);
		Resource newRes = TestUtility.loadOCCIResource(newModelPath);

		Transformator occiToPcg = TransformatorFactory.getTransformator("OCCI2PCG");
		Path pcgPath = Paths.get(TestUtility.getPathToResource("pcg/PCG.pcg"));

		occiToPcg.transform(oldRes, newRes, pcgPath);

		assertTrue(true);
	}
}
