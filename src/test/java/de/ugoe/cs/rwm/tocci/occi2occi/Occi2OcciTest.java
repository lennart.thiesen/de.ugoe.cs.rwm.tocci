/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2occi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.tocci.TestUtility;

public class Occi2OcciTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void differentModelPath() throws EolRuntimeException {
		Path sourceOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Hadoop.occic"));

		OCCI2OCCITransformator trans = new OCCI2OCCITransformator();
		trans.transform(sourceOcci, targetOcci);

		assertTrue(true);
	}

	@Test
	public void duplicateConfiguration() throws EolRuntimeException {
		Path sourceOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Hadoop.occic"));

		OCCI2OCCITransformator trans = new OCCI2OCCITransformator();
		trans.transform(sourceOcci, targetOcci);

		Resource res = TestUtility.loadOCCIResource(targetOcci);
		EList<Configuration> configs = new BasicEList<Configuration>();
		for (EObject obj : res.getContents()) {
			if (obj instanceof Configuration) {
				configs.add((Configuration) obj);
			}
		}
		assertEquals(1, configs.size());
	}

	@Test
	public void duplicateIds() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/duplicateIds.occic"));

		OCCI2OCCITransformator trans = new OCCI2OCCITransformator();
		trans.transform(sourceOcci, targetOcci);

	}

	@Test
	public void emptyLocation() throws EolRuntimeException {
		Path sourceOcci = Paths.get(TestUtility.getPathToResource("occi/emptyLocation.occic"));

		OCCI2OCCITransformator trans = new OCCI2OCCITransformator();
		trans.transform(sourceOcci, targetOcci);

	}
}
