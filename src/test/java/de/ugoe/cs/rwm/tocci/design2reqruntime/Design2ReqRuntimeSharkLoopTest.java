/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.design2reqruntime;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.tocci.TestUtility;
import de.ugoe.cs.rwm.tocci.Transformator;

public class Design2ReqRuntimeSharkLoopTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.FATAL);
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void Shark_VCS() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/SmartSharkLoop.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();
	}
}
