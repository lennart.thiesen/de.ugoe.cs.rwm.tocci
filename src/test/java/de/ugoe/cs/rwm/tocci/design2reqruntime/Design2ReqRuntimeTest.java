/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.design2reqruntime;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.tocci.TestUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import workflow.Task;
import workflow.Taskdependency;

public class Design2ReqRuntimeTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.FATAL);
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void MLS_removeHadoop() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Hadoop.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();

		Path mlsFinished = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_finished.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(mlsFinished);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		if (comp.getMissingElements().isEmpty() && comp.getNewElements().isEmpty()) {
			assertTrue(true);
		} else {
			System.out.println("Missing:");
			for (EObject obj : comp.getMissingElements()) {
				System.out.println(obj);
			}

			System.out.println("New:");
			for (EObject obj : comp.getNewElements()) {
				System.out.println(obj);
			}

			assertTrue(false);
		}
	}

	@Test
	public void Hadoop_Start() throws EolRuntimeException {

		System.out.println("\n\n HADOOP START");
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Hadoop.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_finished.occic"));

		org.eclipse.emf.ecore.resource.Resource designModel = TestUtility.loadOCCIResource(designOcci);
		org.eclipse.emf.ecore.resource.Resource runtimeModel = TestUtility.loadOCCIResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designModel, runtimeModel, targetOcci);

		Path hadoopStart = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/Hadoop_Start.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(hadoopStart);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		if (comp.getMissingElements().isEmpty() && comp.getNewElements().isEmpty()) {
			assertTrue(true);
		} else {
			System.out.println("Missing:");
			for (EObject obj : comp.getMissingElements()) {
				System.out.println(obj);
			}

			System.out.println("New:");
			for (EObject obj : comp.getNewElements()) {
				System.out.println(obj);
			}

			assertTrue(false);
		}
	}

	@Test
	public void MLS_Transfer_Hadoop() throws EolRuntimeException {
		System.out.println("\n\n MLS_Transfer_hadoop");
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Hadoop.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Transfer_Hadoop.occic"));

		org.eclipse.emf.ecore.resource.Resource designModel = TestUtility.loadOCCIResource(designOcci);
		org.eclipse.emf.ecore.resource.Resource runtimeModel = TestUtility.loadOCCIResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designModel, runtimeModel, targetOcci);

		Path hadoopStart = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Transfer_Hadoop_Finished.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(hadoopStart);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		if (comp.getMissingElements().isEmpty() && comp.getNewElements().isEmpty()) {
			assertTrue(true);
		} else {
			System.out.println("Missing:");
			for (EObject obj : comp.getMissingElements()) {
				System.out.println(obj);
			}

			System.out.println("New:");
			for (EObject obj : comp.getNewElements()) {
				System.out.println(obj);
			}

			assertTrue(false);
		}
	}

	@Test
	public void MLS_Mid() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Hadoop.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Mid.occic"));

		org.eclipse.emf.ecore.resource.Resource designModel = TestUtility.loadOCCIResource(designOcci);
		org.eclipse.emf.ecore.resource.Resource runtimeModel = TestUtility.loadOCCIResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designModel, runtimeModel, targetOcci);

		Path hadoopStart = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_finished.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(hadoopStart);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		if (comp.getMissingElements().isEmpty() && comp.getNewElements().isEmpty()) {
			assertTrue(true);
		} else {
			System.out.println("Missing:");
			for (EObject obj : comp.getMissingElements()) {
				System.out.println(obj);
			}

			System.out.println("New:");
			for (EObject obj : comp.getNewElements()) {
				System.out.println(obj);
			}

			assertTrue(false);
		}
	}

	@Test
	public void cleanup() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/MLS_Hadoop.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mls_hadoop/Hadoop_Finished.occic"));

		org.eclipse.emf.ecore.resource.Resource designModel = TestUtility.loadOCCIResource(designOcci);
		org.eclipse.emf.ecore.resource.Resource runtimeModel = TestUtility.loadOCCIResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designModel, runtimeModel, targetOcci);

		Path emptyOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		org.eclipse.emf.ecore.resource.Resource comparedModel = TestUtility.loadOCCIResource(emptyOcci);

		Comparator comp = ComparatorFactory.getComparator("Simple", finishedModel, comparedModel);

		boolean assertion = true;
		for (EObject obj : comp.getMissingElements()) {

			// Network check due to provider network
			if ((obj instanceof Task || obj instanceof Taskdependency
					|| obj.eClass().getName().equals("Network")) == false) {
				System.out.println(obj);
				assertion = false;
			}
		}

		for (EObject obj : comp.getNewElements()) {
			// Network check due to provider network
			if ((obj instanceof Task || obj instanceof Taskdependency
					|| obj.eClass().getName().equals("Network") == false)) {
				System.out.println(obj);
				assertion = false;
			}
		}

		assertTrue(assertion);
	}
}
