/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci;

import java.io.File;
import java.nio.file.Path;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OCCIResourceFactoryImpl;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.modmacao.ansibleconfiguration.AnsibleconfigurationPackage;
import org.modmacao.occi.platform.PlatformPackage;
import org.modmacao.placement.PlacementPackage;

import modmacao.ModmacaoPackage;
import monitoring.MonitoringPackage;
import openstackruntime.OpenstackruntimePackage;
import ossweruntime.OssweruntimePackage;
import workflow.WorkflowPackage;

public class TestUtility {

	public static String getPathToResource(String resourceName) {
		try {
			return ClassLoader.getSystemClassLoader().getResource(resourceName).getFile();
		} catch (NullPointerException e) {
			// fail("Resource "+resourceName+" could not be found in resource folder!");
			return null;
		}
	}

	public static void extensionRegistrySetup() {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);

		InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		ModmacaoPackage.eINSTANCE.eClass();
		OpenstackruntimePackage.eINSTANCE.eClass();
		PlacementPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		OssweruntimePackage.eINSTANCE.eClass();
		AnsibleconfigurationPackage.eINSTANCE.eClass();
		MonitoringPackage.eINSTANCE.eClass();
		PlatformPackage.eINSTANCE.eClass();

		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/monitoring#",
				OCCIPackage.class.getClassLoader().getResource("model/monitoring.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/modmacao#",
				ModmacaoPackage.class.getClassLoader().getResource("model/modmacao.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/runtime#",
				OpenstackruntimePackage.class.getClassLoader().getResource("model/openstackruntime.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/swe#",
				OssweruntimePackage.class.getClassLoader().getResource("model/openstackruntime.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/placement#",
				PlacementPackage.class.getClassLoader().getResource("model/placement.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure#",
				InfrastructurePackage.class.getClassLoader().getResource("model/Infrastructure.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/core#",
				OCCIPackage.class.getClassLoader().getResource("model/Core.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/workflow#",
				OCCIPackage.class.getClassLoader().getResource("model/workflow.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/swe#",
				OCCIPackage.class.getClassLoader().getResource("model/ossweruntime.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/platform#",
				OCCIPackage.class.getClassLoader().getResource("model/platform.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/ansible#",
				OCCIPackage.class.getClassLoader().getResource("model/ansibleconfiguration.occie").toString());
	}

	public static Resource loadOCCIResource(Path configuration) {
		OCCIPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("occie", new OCCIResourceFactoryImpl());
		m.put("occic", new OCCIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		String file = new File(configuration.toString()).getAbsolutePath();

		Resource resource = resSet.getResource(URI.createFileURI(file), true);

		EcorePlugin.ExtensionProcessor.process(null);
		EcoreUtil.resolveAll(resSet);

		return resource;
	}
}
