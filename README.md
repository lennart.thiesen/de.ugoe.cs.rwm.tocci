# TOCCI

[![pipeline status](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci/badges/master/pipeline.svg)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci/commits/master)
[![coverage report](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci/badges/master/coverage.svg)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci/commits/master)

TOCCI is a library consisting of Model Transformations for OCCI models.
The transformations itself are written in EOL.

Currently the following transformations are part of TOCCI:

## OCCI to OCCI
This transformation adds obvious information to OCCI models and checks whether some information that may be required are filled.
For example, it checks whether locations or OCCI attributes like source and target are set correctly.

## OCCI to Openstack
This transformation adds cloud specific information to a cloud independent OCCI model.
For example, a management network can be added, as well as Userdata, SSH keys and links to management networks for virtual machines.

## OCCI to POG
This transformation creates a Provisioning Order Graph (POG) out of an OCCI model.
This is a graph depicting the dependencies of the modeled cloud elements

## POG to Provisioning Plan
This transformation generates a provisioning plan in form of an UML activity diagram from the POG and its corresponding OCCI model.
